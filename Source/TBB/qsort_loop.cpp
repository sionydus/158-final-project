#include <stdio.h>
#include <stdlib.h>
#include "tbb/tick_count.h"
#include "tbb/task_scheduler_init.h"
#include "tbb/parallel_for.h"
#include <algorithm>
#include <iostream>
#include <fstream>


using namespace std;
using namespace tbb;

// The Quicksort range shown in Example 11-31 is where the partitioning step is done (using std::swap).
// Partitioning involves making sure that all the numbers on one side of a pivot are smaller than or
// equal to the pivot, and the numbers on the other side are larger than or equal to it. Without
// this step, you would end up with a bunch of sort sections in the array. They appear sorted
// overall only because of this partitioning

template<typename RandomAccessIterator, typename Compare>
struct quick_sort_range {
   static const size_t grainsize = 500;
   const Compare &comp;
   RandomAccessIterator begin;
   size_t size;

   quick_sort_range( RandomAccessIterator begin_,
                     size_t size_,
                     const Compare &comp_ ) :
      comp(comp_), begin(begin_), size(size_) {}

   bool empty() const {return size==0;}
   bool is_divisible() const {return size>=grainsize;}
   
  quick_sort_range( quick_sort_range& range, tbb::split) : comp(range.comp) {
      RandomAccessIterator array = range.begin;
      RandomAccessIterator key0 = range.begin;
      size_t m = range.size/2u;
      std::swap ( array[0], array[m] );

      size_t i=0;
      size_t j=range.size;
      // Partition interval [i+1,j-1] with key *key0.
      for(;;) {
	__TBB_ASSERT( i<j, NULL );
	// Loop must terminate since array[l]==*key0. 
	do {
	  --j;
	  __TBB_ASSERT( i<=j, "bad ordering relation?" ); 
	}while( comp( *key0, array[j] ));
   
	do {
	  __TBB_ASSERT( i<=j, NULL ); 
	  if( i==j ) goto partition; 
	  ++i;
	} while( comp( array[i],*key0 ));

	if( i==j ) goto partition;
	std::swap( array[i], array[j] );
      }
partition:

      // Put the partition key where it belongs
      std::swap( array[j], *key0 );
      // array[l..j) is less or equal to key.
      // array(j..r) is greater than or equal to key.
      // array[j] is equal to key
      i=j+1;
      begin = array+i;
      size = range.size-i;
      range.size = j;
   }
};

template<typename RandomAccessIterator, typename Compare>
struct quick_sort_body {
void operator()( const quick_sort_range<RandomAccessIterator, Compare>& range ) const {
      //SerialQuickSort( range.begin, range.size, range.comp );
      std::sort( range.begin, range.begin + range.size, range.comp );
   }
};

template<typename RandomAccessIterator, typename Compare>
void parallel_quick_sort( RandomAccessIterator begin,
                          RandomAccessIterator end,
                          const Compare& comp ) {
  tbb::parallel_for( quick_sort_range<RandomAccessIterator,Compare>
		     (begin, end-begin, comp ), 
		     quick_sort_body<RandomAccessIterator,Compare>() );
}


template<typename RandomAccessIterator, typename Compare>
void parallel_sort( RandomAccessIterator begin,
                    RandomAccessIterator end, 
		    const Compare& comp ) {
   const int min_parallel_size = 500;
   if( end > begin ) {
      if (end - begin < min_parallel_size) {
       std::sort(begin, end, comp);
      } else {
       parallel_quick_sort(begin, end, comp);
      }
   }
}

template<typename RandomAccessIterator>
inline void parallel_sort( RandomAccessIterator begin,
                           RandomAccessIterator end ) {
   parallel_sort( begin, end, std::less< typename std::iterator_traits<RandomAccessIterator>::value_type >() );
}

template<typename T>
inline void parallel_sort( T * begin, T * end ) {
   parallel_sort( begin, end, std::less< T >() );
}



int main(int argc, char* argv[])
{
   int i,n, num, nthreads;
   ifstream file;
   int *array;

   if(!(1 < argc && argc < 4)) {
       fprintf(stderr, "Usage: %s <input_file> [thread_num]\n", argv[0]);
       return -1;
   }
   else if(argc > 2) {
       nthreads = atoi( argv[2] );
   }
   else {
       nthreads = tbb::task_scheduler_init::default_num_threads();
   }

   file.open(((string)argv[1]).data(), ios::in);
   if(file != NULL) {
       //get size of input array
       file >> n;
       array = new int[n];
       //store integers into array.
       for(i = 0; i < n && file >> array[i]; i++);
       file.close();
   }
   else { //file opening failed
       fprintf(stderr, "Unable to open %s\n", argv[1]);
       return -1;
   }



   //   printf("%d threads used\n", nthreads);

   tbb::task_scheduler_init init(nthreads);

   //start timer and call quicksort
   tbb::tick_count t0 = tbb::tick_count::now();
   parallel_sort(array,array+n);
   tbb::tick_count t1 = tbb::tick_count::now();
 
   double timespend = ((t1-t0).seconds());
   printf("%lf\n", timespend);

   //print_array(array, n);

   delete[] array;

}
