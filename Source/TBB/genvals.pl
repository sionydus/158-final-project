#! /usr/bin/perl 

use warnings;
use strict;

die "Usage: perl $0 <size_of_input>\n" if(scalar(@ARGV) != 1);

my $SIZE = $ARGV[0];

open CREATEDFILE, ">in$SIZE.txt" or die $!;

print CREATEDFILE $SIZE , "\n";
for(my $i = 0; $i < $SIZE - 1; $i++) {
    print CREATEDFILE int(rand($SIZE)) , " ";
}
print CREATEDFILE int(rand($SIZE)) , "\n";


close(CREATEDFILE);

