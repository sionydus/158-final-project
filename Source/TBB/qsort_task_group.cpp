#include <stdio.h>
#include <stdlib.h>
#include "tbb/tick_count.h"
#include "tbb/task_scheduler_init.h"
#include "tbb/parallel_invoke.h"
#include "tbb/task_group.h"
#include <iostream>
#include <fstream>
#include <algorithm>

#define QUICKSORT_CUTOFF 500

using namespace std;

int *array;

// Choose median of three keys
template <class T>
T* median_of_three(T* x, T* y, T* z) {
   return *x<*y ? *y<*z ? y : *x<*z ? z : x
      : *z<*y ? y : *z<*x ? z : x;
}

// Choose a partition key as median of medians
template <class T>
T* choose_partition_key( T* first, T* last ) { 
   size_t offset = (last-first)/8;
   return median_of_three(
      median_of_three(first, first+offset, first+offset*2),
      median_of_three(first+offset*3, first+offset*4, last-(3*offset+1)),
      median_of_three(last-(2*offset+1), last-(offset+1), last-1 )
   );
}

template <class T>
T* divide( T* first, T* last ) { // Move partition key to front
   std::swap( *first, *choose_partition_key(first,last) );
   // Partition
   T key = *first;
   T* middle = std::partition( first+1, last, [=](const T& x) {return x<key;} ) - 1;
   if( middle!=first ) {
      // Move partition key to between the partitions
      std::swap( *first, *middle );
   }
   else {
     // Check if all keys are equal
     if( last==std::find_if( first+1, last, [=](const T& x) {return key<x;} ) ) return NULL;
   }
   return middle;
}

template <class T>
void quicksort( T* first, T*last) {

tbb::task_group g;

    while( last - first > QUICKSORT_CUTOFF) {
	T* middle = divide(first, last);
	if( !middle ) {
	    g.wait();
	    return;
	}
// Now have two subproblems: [ first .. middle) and [middle+1.. last )
	if( middle-first < last-(middle+1) ) { 
// Left problem ( first .. middle) is smaller , so spawn it .
	g.run([=]{quicksort( first, middle );}); // Solve right subproblem in next iteration .
	first = middle+1; } else {
// Right problem (middle .. last ) is smaller , so spawn it .
 	g.run([=]{quicksort( middle+1, last );}); // Solve left subproblem in next iteration .
 	last = middle; }
    }
// Base case
    std::sort(first,last);
    g.wait();
}

int main(int argc, char **argv) {
    
    int i,n, num, nthreads;
    ifstream file;
    int *array;

    if(!(1 < argc && argc < 4)) {
	fprintf(stderr, "Usage: %s <input_file> [thread_num]\n", argv[0]);
	return -1;
    }
    else if(argc > 2) {
	nthreads = atoi( argv[2] );
    }
    else {
	nthreads = tbb::task_scheduler_init::default_num_threads();
    }

    file.open(((string)argv[1]).data(), ios::in);
    if(file != NULL) {
	//get size of input array
	file >> n;
	array = new int[n];
	//store integers into array.
	for(i = 0; i < n && file >> array[i]; i++);
	file.close();
    }
    else { //file opening failed
	fprintf(stderr, "Unable to open %s\n", argv[1]);
	return -1;
    }



    //    printf("%d threads used\n", nthreads);

    tbb::task_scheduler_init init(nthreads);

    //start timer and call quicksort
    tbb::tick_count t0 = tbb::tick_count::now();
    quicksort(array, array + n);
    tbb::tick_count t1 = tbb::tick_count::now();
 
    double timespend = ((t1-t0).seconds());
    printf("%lf\n", timespend);

    //print_array(array, n);

    delete[] array;


    return 0;
}
