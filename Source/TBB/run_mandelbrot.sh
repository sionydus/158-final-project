#!/bin/bash


if [ $# -ne 2 ]; then
	echo "Usage: $0 <num of points> <num of iterations>"
	exit 0
fi

# get arguments
NTH=$1
NI=$2

# filename partition_type
function run16 {
	for i in {1..32}; do
		$1 $NTH $NI $2 $i
	done
}


make -f Makefile_csif mandelbrot

#chmod +x mandelbrot_r mandelbrot_s mandelbrot_g mandelbrot_d


echo "----------"
echo "FIX"
echo "----------"
run16 "./mandelbrot" 0

echo "----------"
echo "AUTO"
echo "----------"
run16 "./mandelbrot" 1

echo "----------"
echo "AFFINITY"
echo "----------"
run16 "./mandelbrot" 2
