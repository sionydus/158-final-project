#include <stdio.h>
#include <stdlib.h>
#include "tbb/tick_count.h"
#include "tbb/task_scheduler_init.h"
#include "tbb/parallel_invoke.h"
#include "tbb/spin_mutex.h"
#include "tbb/mutex.h"
#include <iostream>
#include <fstream>
#include <random>

using namespace std;

int *array;
static tbb::spin_mutex mutex;
std::default_random_engine rand_engine;

template<class T>
T partition(T b, T e) {

    std::uniform_int_distribution<int> rand_distribution(0, std::distance(b, e - 1));

    // call static random engine to get index of random pivot
    int rand_pivot_index;
    {
	tbb::spin_mutex::scoped_lock lock(mutex);
	rand_pivot_index = rand_distribution(rand_engine);
    }

    // put pivot to last place in range
    std::swap(*(b + rand_pivot_index), *(e - 1));

    // save pivot value
    auto pivot = *(e - 1);
    auto pivotiterator = b;

    // sort the range
    for(auto it = b; it != e - 1; ++it) {
        if(*it < pivot) {
            std::swap(*it, *pivotiterator);
            ++pivotiterator;
        }
    }

    // put pivot to its according position and return position
    std::swap(*(pivotiterator), *(e - 1));
    return pivotiterator;
}


template<class T>
void quick_sort_parallel(T b, T e) {
    if (b != e) {
        T m = partition(b, e);

        // switch to different sort on worst case or smaller ranges
        // if(std::distance(b, m) > 500) {
            tbb::parallel_invoke([&] { quick_sort_parallel(b, m); }, 
                                 [&] { quick_sort_parallel(m + 1, e); });
        // }
        // else 
        //     std::sort(b, e); //defined somewhere else, pretty standard
    }
}

int main(int argc, char**argv) {
    
   int i,n, num, nthreads;
   ifstream file;
   int *array;

   if(!(1 < argc && argc < 4)) {
       fprintf(stderr, "Usage: %s <input_file> [thread_num]\n", argv[0]);
       return -1;
   }
   else if(argc > 2) {
       nthreads = atoi( argv[2] );
   }
   else {
       nthreads = tbb::task_scheduler_init::default_num_threads();
   }

   file.open(((string)argv[1]).data(), ios::in);
   if(file != NULL) {
       //get size of input array
       file >> n;
       array = new int[n];
       //store integers into array.
       for(i = 0; i < n && file >> array[i]; i++);
       file.close();
   }
   else { //file opening failed
       fprintf(stderr, "Unable to open %s\n", argv[1]);
       return -1;
   }



   //   printf("%d threads used\n", nthreads);

   tbb::task_scheduler_init init(nthreads);

   //start timer and call quicksort
   tbb::tick_count t0 = tbb::tick_count::now();
   quick_sort_parallel(array, array + n);
   tbb::tick_count t1 = tbb::tick_count::now();
 
   double timespend = ((t1-t0).seconds());
   printf("%lf\n", timespend);

   //print_array(array, n);

   delete[] array;    

    return 0;
}
