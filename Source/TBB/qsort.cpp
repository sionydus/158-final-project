#include <stdio.h>
#include <stdlib.h>
#include "tbb/tick_count.h"
#include "tbb/task_scheduler_init.h"
#include "tbb/parallel_invoke.h"
#include <iostream>
#include <fstream>

using namespace std;

void quicksort(int *array, int p, int r);


void swap(int *i, int *j) {  
   int tmp = *i;
   *i = *j;
   *j = tmp;
}

int separate(int *x, int low, int high)  {  
   int i, pivot, last;

   pivot = x[low];  
   swap(x + low, x + high);
   last = low;
   for (i = low; i < high; i++) {
      if (x[i] <= pivot) {
         swap(x + last, x + i);
         last += 1;
      }
   }
   swap(x + last, x + high);
   return last;
}


class QuicksortFunctor {
public:
    int* array;
    int p;
    int r;

    // Constructor
    QuicksortFunctor(int* array, int p, int r): array(array), p(p), r(r) {};

    void operator() () const {
	quicksort(array,p,r);
    }
};

void quicksort(int *array, int p, int r) {
    if (p < r) {
        int q = separate(array, p, r);
        
        QuicksortFunctor left (array,p,q-1);
        QuicksortFunctor right(array,q+1,r);
        tbb::parallel_invoke(left,right);
    }
}


void print_array(int *array, int size) {
  int i;
  for (i=0;i<size;i++)
    printf("%d\n",array[i]);
}

int main(int argc, char* argv[])
{
   int i,n, num, nthreads;
   ifstream file;
   int *array;

   if(!(1 < argc && argc < 4)) {
       fprintf(stderr, "Usage: %s <input_file> [thread_num]\n", argv[0]);
       return -1;
   }
   else if(argc > 2) {
       nthreads = atoi( argv[2] );
   }
   else {
       nthreads = tbb::task_scheduler_init::default_num_threads();
   }

   file.open(((string)argv[1]).data(), ios::in);
   if(file != NULL) {
       //get size of input array
       file >> n;
       array = new int[n];
       //store integers into array.
       for(i = 0; i < n && file >> array[i]; i++);
       file.close();
   }
   else { //file opening failed
       fprintf(stderr, "Unable to open %s\n", argv[1]);
       return -1;
   }



   //   printf("%d threads used\n", nthreads);

   tbb::task_scheduler_init init(nthreads);

   //start timer and call quicksort
   tbb::tick_count t0 = tbb::tick_count::now();
   quicksort(array,0,n-1);
   tbb::tick_count t1 = tbb::tick_count::now();
 
   double timespend = ((t1-t0).seconds());
   printf("%lf\n", timespend);

   //print_array(array, n);

   delete[] array;

}
