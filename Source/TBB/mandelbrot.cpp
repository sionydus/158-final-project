/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 * Mandelbrot Generator on Core Duo with TBB                             *
 * Copyright (C) 2007, Sun Wei                                           *
 *                                                                       *
 * This program is free software; you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation; either version 2 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * This program is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * The author of this program may be contacted at quickwayne@gmail.com   *
 * http://quickwayne.googlepages.com                                     *
 *                                                                       *
 \* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// -------------------------------------------------------------------------------------
// QuickMAN.c -- Main file for the QuickMAN SSE/SSE2-based Mandelbrot Set calculator
// Copyright (C) 2006 Paul Gentieu (paul.gentieu@yahoo.com)
//
// This file is part of QuickMAN.
//
// QuickMAN is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
//
// Project Page: http://sourceforge.net/projects/quickman
//
// Author: Paul Gentieu (main code, ASM iteration cores, palettes)
//
// Contributors:
// Andy Milne
// George Bullis
// Mike Mossey (palettes)
// Ben Valdes (fast "wave" algorithm)
// Luciano Genero (conversion to C)
// Peter Kankowski (general suggestions)
//
// Thanks to Daniele Paccaloni and the other FFFF authors for the zooming
// user interface (duplicated in QuickMAN).



/*-----------------------------------------------------------------------------
 *  settings
 *-----------------------------------------------------------------------------*/

// whether to use complex library
#define USING_COMPLEX

// type of partitioner
//#define USING_FIX_GRAIN
//#define USING_AUTO_PARTITION

enum{
	FIX_GRAIN,
	AUTO_PARTITION,
	AFFINITY_PARTITION
};

#ifdef USING_COMPLEX
#include <complex>
#endif

#include <stdio.h>
#include <stdlib.h>

#include "tbb/task_scheduler_init.h"
#include "tbb/blocked_range.h"
#include "tbb/parallel_for.h"
#include "tbb/tick_count.h"

using namespace tbb;


uint XSIZE, YSIZE;

//#ifdef USING_FIX_GRAIN
//#define GRAIN 64
int GRAIN = 64; // default grain size
//#endif

unsigned int iterctr=0;
uint maxiters;

void mandel_double(double fx, double fy, unsigned int* pb) {
	double a, b, x, y, xx, yy, rad;
	unsigned iters, iter_ct;

	iters = 0;
//	iter_ct = 256;
	iter_ct = maxiters;

	a = fx;
	b = fy;
	rad = 4.0;
	x = y = xx = yy = 0.0;

	do
	{
		y = (x + x) * y + b;
		x = xx - yy + a;
		yy = y * y;
		xx = x * x;
		iters++;
		if ((xx + yy) >= rad) {
			*pb = iters;
			iterctr += iters;
			return;
		}
	}
	while (--iter_ct);

	iterctr += iters;
	*pb=0;
	return;

}

#ifdef USING_COMPLEX
// matloff function
int inset(std::complex<double> c) {
	int iters;
	float rl,im;
	std::complex<double> z = c;

//	uint maxiters = 256;

	for (iters = 0; iters < maxiters; iters++) {
		z = z*z + c;
		rl = z.real();
		im = z.imag();
		if (rl*rl + im*im > 4) return 0;
	}                                                                                 
	return 1;
}
#endif


double *real; // real part
double *image; // imaginary

int *begin_list;

struct CBlock {
	unsigned int* pblock;
	int xsize;

	void operator() ( const blocked_range<int>& range ) const {
		double fy;
		int x;
		unsigned int* pb;

		pb = pblock + xsize*range.begin();

		for (int i=range.begin(); i!=range.end(); ++i) {
			fy = image[i];
			begin_list[i]=range.begin();
			for( x=0; x < xsize ; x++) {
#ifdef USING_COMPLEX
				std::complex<double> z = real[x] + fy * std::complex<double>(0, 1);
				*pb = inset(z);
#else
				mandel_double(real[x], fy, pb);
#endif
				pb++;
			}
		}
		//		printf("-->From %d to %d\n", range.begin(), range.end());
	}
};

void buildmandel(double r, double j, double mag, unsigned int *pblock, int partitioning) {
	int x,y;
//	double inc;
	CBlock block;
	int ysize=YSIZE;

//	inc = 2.5/(double)YSIZE/mag;

//	for (x=0, r-=inc*(XSIZE/2); x<XSIZE; x++, r+=inc) real[x]=r;
//	for (y=0, j-=inc*(YSIZE/2); y<YSIZE; y++, j+=inc) image[y]=j;

	// calculate the interesting area, same as OMP ver.
	float xhalf = XSIZE/2.0;
	float yhalf = YSIZE/2.0;
	for (x=0; x<XSIZE; x++) real[x]=(x - xhalf) / xhalf * 2;
	for (y=0; y<YSIZE; y++) image[y]=(y - yhalf) / yhalf * 2;

	block.xsize = XSIZE;
	block.pblock = pblock;

	switch(partitioning){
		case FIX_GRAIN:
			parallel_for(blocked_range<int>(0, ysize, GRAIN), block);
			break;

		case AUTO_PARTITION:
			parallel_for(blocked_range<int>(0, ysize), block, auto_partitioner());
			break;

		case AFFINITY_PARTITION:
			static affinity_partitioner ap;
			parallel_for(blocked_range<int>(0, ysize), block, ap);
			break;
	}

	return;

}


extern unsigned pal_3[];

int main(int argc, char *argv[]) {
	// argument checking
	if(5 > argc || argc > 6){
		fprintf(stderr, "Usage: %s <dimension> <number of iterations> <partitioning type> <num of threads> [<grain size>]\n", argv[0]);
		exit(1);
	}

	XSIZE = YSIZE = atoi(argv[1]); // square area
	real = new double[XSIZE]; // real part
	image = new double[YSIZE]; // imaginary
	begin_list = new int[YSIZE];


	unsigned int* pblock;
	double x, y, mag;

	// number of iterations
	uint niter = atoi(argv[2]);
	maxiters = niter;

	// partitioning type
	int partitioning = atoi(argv[3]);

	// num of threads
	task_scheduler_init init(atoi(argv[4]));

	// grain size
	if(argc == 6)
		GRAIN = atoi(argv[5]);

	x = -0.65; y = 0;
	mag=1.1; // 12;

	pblock = (unsigned int*)malloc(sizeof(int)*XSIZE*YSIZE);
	if (!pblock) {
		printf("Error in malloc\n");
		exit(1);
	}

//	switch(partitioning){
//		case FIX_GRAIN:
//			printf("Mandelbrot set with TBB with grain %d\n", GRAIN); 
//			break;
//
//		case AUTO_PARTITION:
//			printf("Mandelbrot set with TBB with auto parition\n"); 
//			break;
//
//		case AFFINITY_PARTITION:
//			printf("Mandelbrot set with TBB with affinity parition\n"); 
//			break;
//	}

//	printf("Building Mandelbrot Set of size %d for %d iterations...\n", XSIZE, niter);

	tbb::tick_count t0 = tbb::tick_count::now();
//	for (int mandelcount=0; mandelcount< niter; mandelcount++) {
	buildmandel(x, y, mag, pblock, partitioning);
//	}
	tbb::tick_count t1 = tbb::tick_count::now();

	double timespend = ((t1-t0).seconds());

//	printf("Done! It took %f second for %d iterations.\n", timespend, niter);
	printf("%f\n", timespend);

	free(pblock);

	return 0;
}
