#include <stdio.h>
#include <stdlib.h>
#include "tbb/tick_count.h"
#include "tbb/task_scheduler_init.h"
#include "tbb/parallel_invoke.h"
#include <iostream>
#include <fstream>
#include <algorithm>

using namespace std;

template <class T>

void ParallelQuicksort( T* begin, T* end ) {
   if( end - begin > 1 ) {
//       using namespace std;
       T* mid = partition( begin+1, end, bind2nd(less<T>(),*begin) );
       swap( *begin, mid[-1] );
       //[=] and everything after is a lambda expression
       tbb::parallel_invoke( [=]{ParallelQuicksort( begin, mid-1 );},
                             [=]{ParallelQuicksort( mid, end );} );
   }
}

int * array;

int main(int argc, char ** argv) {

   int i,n, num, nthreads;
   ifstream file;
   int *array;

   if(!(1 < argc && argc < 4)) {
       fprintf(stderr, "Usage: %s <input_file> [thread_num]\n", argv[0]);
       return -1;
   }
   else if(argc > 2) {
       nthreads = atoi( argv[2] );
   }
   else {
       nthreads = tbb::task_scheduler_init::default_num_threads();
   }

   file.open(((string)argv[1]).data(), ios::in);
   if(file != NULL) {
       //get size of input array
       file >> n;
       array = new int[n];
       //store integers into array.
       for(i = 0; i < n && file >> array[i]; i++);
       file.close();
   }
   else { //file opening failed
       fprintf(stderr, "Unable to open %s\n", argv[1]);
       return -1;
   }

   //   printf("%d threads used\n", nthreads);

   tbb::task_scheduler_init init(nthreads);

   tbb::tick_count t0 = tbb::tick_count::now();
   ParallelQuicksort(array, array+n);
   tbb::tick_count t1 = tbb::tick_count::now();

   double timespend = ((t1-t0).seconds());
   printf("%lf\n", timespend);

   //print_array(array, n);

   delete[] array;

   return 0;
}
