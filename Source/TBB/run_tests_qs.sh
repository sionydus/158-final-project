#! /bin/bash

if [ $# -ne 1 ]
  then echo "Usage: $0 <input_file>"
  exit 0
fi

make -f Makefile clean
make -f Makefile

echo "----------"
echo "qsort"
echo "----------"
for i in {1..16}; do
   ./qsort $1 $i
done

echo "----------"
echo "qsort2"
echo "----------"
for i in {1..16}; do
   ./qsort2 $1 $i
done

echo "----------"
echo "qsort2_500"
echo "----------"
for i in {1..16}; do
   ./qsort2_500 $1 $i
done

echo "----------"
echo "qsort2_mutex_pivot"
echo "----------"
for i in {1..16}; do
   ./qsort2_mutex_pivot $1 $i
done

echo "----------"
echo "qsort_loop"
echo "----------"
for i in {1..16}; do
   ./qsort_loop $1 $i
done

echo "----------"
echo "qsort_task_group"
echo "----------"
for i in {1..16}; do
   ./qsort_task $1 $i
done

echo "----------"
echo "qsort_task"
echo "----------"
for i in {1..16}; do
   ./qsort_task $1 $i
done

echo "----------"
echo "qsort_tbb_builtin"
echo "----------"
for i in {1..16}; do
   ./qsort_tbb_builtin $1 $i
done
