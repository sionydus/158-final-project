#include <iostream>
#include <fstream>
#include <sys/time.h>
#include "tbb/tick_count.h"
#include "tbb/task_scheduler_init.h"
#include "tbb/parallel_sort.h"

using namespace std;

int *array;

void print_array(int *array, int size) {
  int i;
  for (i=0;i<size;i++)
    printf("%d\n",array[i]);
}


int main(int argc, char** argv) {
   int i,n, num, nthreads;
   ifstream file;

   if(!(1 < argc && argc < 4)) {
       fprintf(stderr, "Usage: %s <input_file> [thread_num]\n", argv[0]);
       return -1;
   }
   else if(argc > 2) {
       nthreads = atoi( argv[2] );
   }
   else {
       nthreads = tbb::task_scheduler_init::default_num_threads();
   }

   file.open(((string)argv[1]).data(), ios::in);
   if(file == NULL) {
       fprintf(stderr, "Unable to open %s\n", argv[1]);
       return 1;
   }

   //get size of input array
   file >> n;
   array = new int[n];

   for(i = 0; i < n && file >> array[i]; i++);
   file.close();

   //   cout << nthreads << " threads used" << endl;

   tbb::task_scheduler_init init(nthreads);
        
   tbb::tick_count t0 = tbb::tick_count::now();
   tbb::parallel_sort<int>(array, array + n);
   tbb::tick_count t1 = tbb::tick_count::now();

   double timespend = ((t1-t0).seconds());
   printf("%lf\n", timespend);

   //print_array(array, n);

   delete[] array;

   return 0;
}

