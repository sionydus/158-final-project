/*
  author: jbenet
  os x, compile with: gcc -o testo test.c
  linux, compile with: gcc -o testo test.c -lrt
*/
 
#include <time.h>
#include <sys/time.h>
#include <stdio.h>
 
#ifdef __MACH__
#include <mach/clock.h>
#include <mach/mach.h>
#endif
 
 
void current_utc_time(struct timespec *ts) {
 
#ifdef __MACH__ // OS X does not have clock_gettime, use clock_get_time
    clock_serv_t cclock;
    mach_timespec_t mts;
    host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
    clock_get_time(cclock, &mts);
    mach_port_deallocate(mach_task_self(), cclock);
    ts->tv_sec = mts.tv_sec;
    ts->tv_nsec = mts.tv_nsec;
#else
    clock_gettime(CLOCK_REALTIME, ts);
#endif
 
}

float timediff(struct timespec t1, struct timespec t2)  {  
    if (t1.tv_nsec > t2.tv_nsec) {
	t2.tv_sec -= 1;
	t2.tv_nsec += 1000000000;
    }
    return t2.tv_sec-t1.tv_sec + 0.000000001 * (t2.tv_nsec-t1.tv_nsec);
}
 
 
/* int main(int argc, char **argv) { */
 
/*     struct timespec ts; */
/*     current_utc_time(&ts); */
 
/*     printf("s: %lu\n", ts.tv_sec); */
/*     printf("ns: %lu\n", ts.tv_nsec); */
/*     return 0; */
 
/* } */
