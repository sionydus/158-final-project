#!/bin/bash


if [ $# -ne 2 ]; then
	echo "Usage: $0 <num of points> <num of iterations>"
	exit 0
fi

# get arguments
NTH=$1
NI=$2

function run32 {
	for i in {1..32}; do
		$1 $NTH $NI $i
	done
}


make mandelbrot

#chmod +x mandelbrot_r mandelbrot_s mandelbrot_g mandelbrot_d

echo "----------"
echo "RC"
echo "----------"
run32 "./mandelbrot_r"

echo "----------"
echo "STATIC"
echo "----------"
run32 "./mandelbrot_s"

echo "----------"
echo "GUIDED"
echo "----------"
run32 "./mandelbrot_g"

echo "----------"
echo "DYNAMIC"
echo "----------"
run32 "./mandelbrot_d"
