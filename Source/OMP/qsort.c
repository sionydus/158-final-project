#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include "current_utc_time.h"

// OpenMP example program:  quicksort; not necessarily efficient

void swap(int *yi, int *yj)
{  int tmp = *yi;
   *yi = *yj;
   *yj = tmp;
}

int separate(int *x, int low, int high)
{  int i,pivot,last;
   pivot = x[low];
   swap(x+low,x+high);
   last = low;
   for (i = low; i < high; i++) {
      if (x[i] <= pivot) {
         swap(x+last,x+i);
         last += 1;
      }
   }
   swap(x+last,x+high);
   return last;
}

// quicksort of the array z, elements zstart through zend; set the
// latter to 0 and m-1 in first call, where m is the length of z;
// firstcall is 1 or 0, according to whether this is the first of the
// recursive calls
void qs(int *z, int zstart, int zend, int firstcall)
{
   #pragma omp parallel
   {  int part;
      if (firstcall == 1) {
         #pragma omp single nowait
         qs(z,0,zend,0);
      } else {
         if (zstart < zend) {
            part = separate(z,zstart,zend);
            #pragma omp task
            qs(z,zstart,part-1,0);
            #pragma omp task
            qs(z,part+1,zend,0);
         }

      }
   }
}

// test code
int main(int argc, char**argv)
{
   int i, n, num, *array;
   FILE * file;

   if(argc != 2) {
       fprintf(stderr, "Usage: %s <input_file_with_numbers>\n", argv[0]);
       return -1;
   }

   file = fopen(argv[1], "r");

   if(file != NULL) {
       fscanf(file,"%d",&n);
       array = (int*)malloc(sizeof(int)*n);

       i = 0;
       while(fscanf(file,"%d",&num) > 0)
	   array[i++] = num;

       close(file);
   }

   struct timespec start,end;

   current_utc_time(&start);
   qs(array,0,n-1,1);
   current_utc_time(&end);

   printf("%f\n",timediff(start,end));

   // for (i = 0; i < n; i++) printf("%d\n",array[i]);

   return 0;
}
