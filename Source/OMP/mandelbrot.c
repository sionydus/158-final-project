// compile with -D, e.g.
//
//    gcc -fopenmp -o mandelbrot mandelbrot.c -DDYNAMIC
//
// to get the version that uses dynamic scheduling

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <complex.h>
#include "current_utc_time.h"


#ifdef RC
// finds chunk among 0,...,n-1 to assign to thread number me among nth
// threads
void findmyrange(int n,int nth,int me,int *myrange)
{  int chunksize = n / nth;
   myrange[0] = me * chunksize;
   if (me < nth-1) myrange[1] = (me+1) * chunksize - 1;
   else myrange[1] = n - 1;
}

#include <stdlib.h>
#include <stdio.h>
// from http://www.cis.temple.edu/~ingargio/cis71/code/randompermute.c
// It returns a random permutation of 0..n-1
int * rpermute(int n) {
  int *a = (int *)(int *)  malloc(n*sizeof(int));
  // int *a = malloc(n*sizeof(int));
  int k;
  for (k = 0; k < n; k++)
    a[k] = k;
  for (k = n-1; k > 0; k--) {
    int j = rand() % (k+1);
    int temp = a[j];
    a[j] = a[k];
    a[k] = temp;
  }
  return a;
}
#endif

//#define MAXITERS 1000

unsigned int maxiters;

// globals
int count = 0;
int nptsside;
float side2;
float side4;

int inset(double complex c) {
   int iters;
   float rl,im;
   double complex z = c;
   for (iters = 0; iters < maxiters; iters++) {
      z = z*z + c;
      rl = creal(z);
      im = cimag(z);
      if (rl*rl + im*im > 4) return 0;
   }
   return 1;
}

int *scram;

void dowork()
{
   #ifdef RC
   #pragma omp parallel reduction(+:count)
   #else
   #pragma omp parallel
   #endif
   {
      int x,y; float xv,yv;
      double complex z;
      #ifdef STATIC
      #pragma omp for reduction(+:count) schedule(static)
      #elif defined DYNAMIC
      #pragma omp for reduction(+:count) schedule(dynamic)
      #elif defined GUIDED
      #pragma omp for reduction(+:count) schedule(guided)
      #endif
      #ifdef RC
      int myrange[2];
      int me = omp_get_thread_num();
      int nth = omp_get_num_threads();
      int i;
      findmyrange(nptsside,nth,me,myrange);
      for (i = myrange[0]; i <= myrange[1]; i++) {
         x = scram[i];
      #else
      for (x=0; x<nptsside; x++) {
      #endif
         for ( y=0; y<nptsside; y++)  {
					 // only calculate the interesting area
            xv = (x - side2) / side2 * 2; 
            yv = (y - side2) / side2 * 2;
            z = xv + yv*I;
            if (inset(z)) {
               count++;
            }
         }
      }
   }
}

int main(int argc, char **argv)
{
	// argument checking
	if(3 > argc || argc > 4){
		printf("Usage: %s <nptsside> <maxiters> [<num_threads>]\n", argv[0]);
		exit(1);
	}


   nptsside = atoi(argv[1]);
	 maxiters = atoi(argv[2]);

	 // set num of threads
	 if(argc == 4)
		 omp_set_num_threads(atoi(argv[3]));

   side2 = nptsside / 2.0;
   side4 = nptsside / 4.0;

   struct timespec bgn,nd;

   #ifdef RC
   scram = rpermute(nptsside);
   #endif

   current_utc_time(&bgn);
   dowork();
   current_utc_time(&nd);

//   printf("%d\n",count);

   printf("%f\n",timediff(bgn,nd));

   return 0;
}
